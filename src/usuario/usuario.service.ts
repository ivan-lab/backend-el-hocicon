import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuario } from './entities/usuario.entity';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
  ) {}
  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    try {
      const UsuarioCreada = await this.usuarioRepository.findOne({
        where: { nombreUsuario: createUsuarioDto.nombreUsuario },
      });
      if (UsuarioCreada) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Usuario con el Titulo: ${createUsuarioDto.nombreUsuario} YA EXISTE`,
          message: `La Usuario con el Titulo: ${createUsuarioDto.nombreUsuario} YA FUE CREADO`,
        });
      }
      const Usuario = this.usuarioRepository.create(createUsuarioDto);
      return await this.usuarioRepository.save(Usuario);
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else if (error instanceof UnauthorizedException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Usuario metodo create): ${error}`,
          message: `Error del Servidor en (Usuario metodo create): ${error}`,
        });
      }
    }
  }
  // async seedCreate(): Promise<Usuario[]> {
  //   try {
  //     const usuariosData = [
  //       {
  //         titulo: 'Noticia 1',
  //         imagen: 'imagen_noticia_1.jpg',
  //         fechaPublicacion: new Date(),
  //         lugar: 'Ciudad A',
  //         autor: 'Autor A',
  //         contenido: 'Contenido de la Noticia 1',
  //       },
  //       {
  //         titulo: 'Noticia 2',
  //         imagen: 'imagen_noticia_2.jpg',
  //         fechaPublicacion: new Date(),
  //         lugar: 'Ciudad B',
  //         autor: 'Autor B',
  //         contenido: 'Contenido de la Noticia 2',
  //       },
  //       {
  //         titulo: 'Noticia 3',
  //         imagen: 'imagen_noticia_3.jpg',
  //         fechaPublicacion: new Date(),
  //         lugar: 'Ciudad C',
  //         autor: 'Autor C',
  //         contenido: 'Contenido de la Noticia 3',
  //       },
  //       {
  //         titulo: 'Noticia 4',
  //         imagen: 'imagen_noticia_4.jpg',
  //         fechaPublicacion: new Date(),
  //         lugar: 'Ciudad D',
  //         autor: 'Autor D',
  //         contenido: 'Contenido de la Noticia 4',
  //       },
  //       {
  //         titulo: 'Noticia 5',
  //         imagen: 'imagen_noticia_5.jpg',
  //         fechaPublicacion: new Date(),
  //         lugar: 'Ciudad E',
  //         autor: 'Autor E',
  //         contenido: 'Contenido de la Noticia 5',
  //       },
  //     ];
  //     const noticiasCreadas = await this.usuarioRepository.save(usuariosData);
  //     return noticiasCreadas;
  //   } catch (error) {
  //     throw new InternalServerErrorException({
  //       statusCode: 500,
  //       error: `Error del Servidor en (Noticia método seedCreate): ${error}`,
  //       message: `Error del Servidor en (Noticia método seedCreate): ${error}`,
  //     });
  //   }
  // }

  async findAll(): Promise<Usuario[]> {
    try {
      const Usuarios = await this.usuarioRepository.find();
      if (Usuarios.length === 0) {
        throw new BadRequestException({
          statusCode: 400,
          error: `No se encontraron Usuarios.`,
          message: `No se encontraron Usuarios. A un no se subio.`,
        });
      }
      return Usuarios;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Usuario metodo findAll): ${error}`,
          message: `Error del Servidor en (Usuario metodo findAll): ${error}`,
        });
      }
    }
  }

  async findOne(id: number): Promise<Usuario> {
    try {
      const Usuario = await this.usuarioRepository.findOne({ where: { id } });
      if (!Usuario) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Usuario con ID ${id} NO Existe`,
          message: `La Usuario con ID ${id} no fue encontrado`,
        });
      }
      return Usuario;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Usuario metodo findOne): ${error}`,
          message: `Error del Servidor en (Usuario metodo findOne): ${error}`,
        });
      }
    }
  }

  async update(
    id: number,
    updateUsuarioDto: UpdateUsuarioDto,
  ): Promise<Usuario> {
    try {
      const Usuario = await this.findOne(id);
      this.usuarioRepository.merge(Usuario, updateUsuarioDto);
      const actualizarUsuario = await this.usuarioRepository.save(Usuario);
      return actualizarUsuario;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Usuario método update): ${error}`,
          message: `Error del Servidor en (Usuario método update): ${error}`,
        });
      }
    }
  }

  async remove(id: number): Promise<Usuario> {
    try {
      const Usuario = await this.findOne(id);
      const UsuarioEliminada = { ...Usuario };
      await this.usuarioRepository.remove(Usuario);
      return UsuarioEliminada;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Usuario método remove): ${error}`,
          message: `Error del Servidor en (Usuario método remove): ${error}`,
        });
      }
    }
  }
}
