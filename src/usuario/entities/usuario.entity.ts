import { Entity, PrimaryGeneratedColumn, Column, Unique } from 'typeorm';

@Entity()
@Unique(['nombreUsuario', 'correo'])
export class Usuario {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, unique: true, nullable: false })
  nombreUsuario: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  password: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  nombreCompleto: string;

  @Column({ type: 'varchar', length: 255, unique: true, nullable: false })
  correo: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  rol: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  genero: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  carnet: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  expedido: string;
}
