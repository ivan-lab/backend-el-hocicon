import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as multer from 'multer';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // Configuración básica de Multer para almacenar imágenes en una carpeta
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './uploads/'); // Cambia './uploads/' al directorio que desees
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, file.fieldname + '-' + uniqueSuffix + '.jpg');
    },
  });

  app.use(multer({ storage: storage }).single('imagen'));
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
