import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Noticia {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  titulo: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  imagen?: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  fechaPublicacion: Date;

  @Column({ type: 'varchar', length: 255, nullable: false })
  lugar: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  autor: string;

  @Column({ type: 'text', nullable: false })
  contenido: string;
}
