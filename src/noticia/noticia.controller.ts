import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { NoticiaService } from './noticia.service';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';

@Controller('noticia')
export class NoticiaController {
  constructor(private readonly noticiaService: NoticiaService) {}

  @Post()
  create(@Body() createNoticiaDto: CreateNoticiaDto) {
    return this.noticiaService.create(createNoticiaDto);
  }
  @Post('/seedcreate')
  seedCreate() {
    return this.noticiaService.seedCreate();
  }

  @Get()
  findAll() {
    return this.noticiaService.findAll();
  }
  @Get('/recientes')
  findAllFechasRecientes() {
    return this.noticiaService.findAllFechasRecientes();
  }
  @Get('/antiguas')
  findAllFechasAmtigua() {
    return this.noticiaService.findAllFechasAmtiguas();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.noticiaService.findOne(+id);
  }
  @Get('/lugar/:lugar')
  findLugar(@Param('lugar') lugar: string) {
    return this.noticiaService.findLugar(lugar);
  }
  @Get('/titulo/:titulo')
  findOneTitulo(@Param('titulo') titulo: string) {
    return this.noticiaService.findOneTitulo(titulo);
  }
  @Get('/autor/:autor')
  findAutor(@Param('autor') autor: string) {
    return this.noticiaService.findAutor(autor);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() updateNoticiaDto: UpdateNoticiaDto) {
    return this.noticiaService.update(+id, updateNoticiaDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.noticiaService.remove(+id);
  }
}
