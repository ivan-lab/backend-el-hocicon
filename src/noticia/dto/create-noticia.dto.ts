import {
  IsString,
  IsNotEmpty,
  IsDateString,
  IsOptional,
} from 'class-validator';

export class CreateNoticiaDto {
  @IsString()
  @IsNotEmpty()
  titulo: string;

  @IsString()
  @IsOptional()
  imagen?: string;

  @IsDateString()
  @IsNotEmpty()
  fechaPublicacion: Date;

  @IsString()
  @IsNotEmpty()
  lugar: string;

  @IsString()
  @IsNotEmpty()
  autor: string;

  @IsString()
  @IsNotEmpty()
  contenido: string;
}
