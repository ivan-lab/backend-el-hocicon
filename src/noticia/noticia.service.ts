import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { Noticia } from './entities/noticia.entity';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
@Injectable()
export class NoticiaService {
  constructor(
    @InjectRepository(Noticia)
    private readonly noticiaRepository: Repository<Noticia>,
  ) {}

  async create(createNoticiaDto: CreateNoticiaDto): Promise<Noticia> {
    try {
      const noticiaCreada = await this.noticiaRepository.findOne({
        where: { titulo: createNoticiaDto.titulo },
      });
      if (noticiaCreada) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Noticia con el Titulo: ${createNoticiaDto.titulo} YA EXISTE`,
          message: `La Noticia con el Titulo: ${createNoticiaDto.titulo} YA FUE CREADO`,
        });
      }
      const noticia = this.noticiaRepository.create({
        ...createNoticiaDto,
        fechaPublicacion: new Date(),
        imagen: 'n1.jpg',
      });
      return await this.noticiaRepository.save(noticia);
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else if (error instanceof UnauthorizedException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia metodo create): ${error}`,
          message: `Error del Servidor en (Noticia metodo create): ${error}`,
        });
      }
    }
  }

  async seedCreate(): Promise<Noticia[]> {
    try {
      const noticiasData = [
        {
          titulo: 'Increíble rescate de cachorros en La Paz',
          imagen: 'n1.jpg',
          fechaPublicacion: '2023-02-02 17:37:07.971',
          lugar: 'La Paz',
          autor: 'Ivan',
          contenido:
            'En un emotivo rescate, voluntarios salvaron a una camada de cachorros abandonados en las calles de La Paz. Ivan, un amante de los animales, lideró la operación que ha conmovido a la comunidad.',
        },
        {
          titulo:
            'Adopciones masivas en El Alto: una nueva esperanza para perros callejeros',
          imagen: 'n2.jpg',
          fechaPublicacion: '2023-02-05 09:22:45.512',
          lugar: 'El Alto',
          autor: 'Alcides',
          contenido:
            'Alcides comparte la historia de un evento de adopción masiva que tuvo lugar en El Alto, brindando una nueva oportunidad a perros callejeros. Vecinos se unieron para encontrar hogares amorosos para estos animales necesitados.',
        },
        {
          titulo: 'Rescate heroico de cachorros en La Paz',
          imagen: 'n3.jpeg',
          fechaPublicacion: new Date(),
          lugar: 'La Paz',
          autor: 'Ivan',
          contenido:
            'En una emocionante jornada, Ivan lideró un equipo de rescatistas que salvó a una camada de cachorros abandonados en las caóticas calles de La Paz. El equipo enfrentó diversos desafíos, pero su dedicación y amor por los animales prevalecieron. La comunidad se une para aplaudir esta acción valiente y generar conciencia sobre la importancia de la adopción responsable.',
        },
        {
          titulo:
            'Transformación en El Alto: Programa de adopciones masivas cambia vidas de perros callejeros',
          imagen: 'n4.jpg',
          fechaPublicacion: '2023-02-15 20:45:30.789',
          lugar: 'El Alto',
          autor: 'Alcides',
          contenido:
            'Alcides comparte una conmovedora crónica sobre el programa de adopciones masivas que ha transformado las vidas de perros callejeros en El Alto. A través de historias emotivas y testimonios de adoptantes, la narrativa destaca cómo la comunidad se ha unido para brindar una segunda oportunidad a estos leales compañeros. El evento, liderado por Alcides y su equipo, no solo logró cambiar la vida de los animales, sino que también inspiró a otros a considerar la adopción como una opción viable.',
        },
        {
          titulo: 'Rescate heroico de cachorros en La Paz',
          imagen: 'n5.jpeg',
          fechaPublicacion: '2023-02-15 20:45:30.789',
          lugar: 'La Paz',
          autor: 'Ivan',
          contenido:
            'En una emocionante jornada, Ivan lideró un equipo de rescatistas que salvó a una camada de cachorros abandonados en las caóticas calles de La Paz. El equipo enfrentó diversos desafíos, pero su dedicación y amor por los animales prevalecieron. La comunidad se une para aplaudir esta acción valiente y generar conciencia sobre la importancia de la adopción responsable.',
        },
        {
          titulo:
            'Transformación en El Alto: Programa de adopciones masivas cambia vidas de perros callejeros',
          imagen: 'n6.jpeg',
          fechaPublicacion: '2023-02-20 03:18:12.456',
          lugar: 'El Alto',
          autor: 'Alcides',
          contenido:
            'Alcides comparte una conmovedora crónica sobre el programa de adopciones masivas que ha transformado las vidas de perros callejeros en El Alto. A través de historias emotivas y testimonios de adoptantes, la narrativa destaca cómo la comunidad se ha unido para brindar una segunda oportunidad a estos leales compañeros. El evento, liderado por Alcides y su equipo, no solo logró cambiar la vida de los animales, sino que también inspiró a otros a considerar la adopción como una opción viable.',
        },
        {
          titulo:
            'Historias de amor en Cochabamba: Cuidadores voluntarios crean un refugio para animales abandonados',
          imagen: 'n7.jpg',
          fechaPublicacion: '2023-02-24 12:30:59.123',
          lugar: 'Cochabamba',
          autor: 'Nelly',
          contenido:
            'Nelly nos cuenta la inspiradora historia de un grupo de cuidadores voluntarios en Cochabamba que ha establecido un refugio para animales abandonados. A través de la dedicación de Nelly y su equipo, el refugio se ha convertido en un lugar seguro y amoroso para perros y gatos necesitados. La comunidad se ha unido para apoyar esta iniciativa única que muestra cómo el cuidado y la compasión pueden marcar la diferencia en la vida de los animales sin hogar.',
        },
        {
          titulo:
            'Santa Cruz solidaria: Campaña de esterilización para controlar la población de animales callejeros',
          imagen: 'n8.jpg',
          fechaPublicacion: '2023-02-28 08:07:34.987',
          lugar: 'Santa Cruz',
          autor: 'Cinthya',
          contenido:
            'Cinthya comparte la historia de una campaña de esterilización en Santa Cruz, destinada a controlar la población de animales callejeros y prevenir el sufrimiento innecesario. La iniciativa, liderada por Cinthya y su equipo de voluntarios, ha logrado un impacto significativo en la reducción de la superpoblación de animales. La comunidad celebra esta acción responsable que promueve el bienestar de las mascotas y la convivencia armoniosa entre humanos y animales.',
        },
        {
          titulo: 'Increíbles historias de amor animal en Beni',
          imagen: 'n9.jpg',
          fechaPublicacion: '2023-03-04 22:16:40.345',
          lugar: 'Beni',
          autor: 'Ivan',
          contenido:
            'Ivan nos lleva a un viaje por Beni para explorar las increíbles historias de amor animal que están floreciendo en esta región. Desde rescates heroicos hasta adopciones conmovedoras, la comunidad de Beni demuestra su compromiso con el bienestar de las mascotas. Ivan destaca el papel vital que juegan las personas en la construcción de un entorno seguro y cariñoso para los animales.',
        },
        {
          titulo:
            'Pando solidario: Voluntarios se unen para construir un refugio para animales abandonados',
          imagen: 'n10.jpg',
          fechaPublicacion: '2023-03-09 19:54:18.678',
          lugar: 'Pando',
          autor: 'Alcides',
          contenido:
            'Alcides comparte la inspiradora historia de solidaridad en Pando, donde voluntarios de la comunidad se han unido para construir un refugio para animales abandonados. Este proyecto, liderado por Alcides y su dedicado equipo, busca proporcionar un hogar temporal para aquellos animales que han sido desplazados. La comunidad celebra esta iniciativa que refleja el espíritu solidario de Pando.',
        },
        {
          titulo:
            'Sucre pet-friendly: Parques y servicios adaptados para el bienestar de las mascotas',
          imagen: 'n1.jpg',
          fechaPublicacion: '2023-03-14 06:42:51.890',
          lugar: 'Sucre',
          autor: 'Nelly',
          contenido:
            'Nelly nos invita a descubrir la ciudad de Sucre, donde se han implementado medidas innovadoras para convertirla en un lugar pet-friendly. Desde parques diseñados para mascotas hasta servicios especializados, Sucre se destaca como un ejemplo de cómo las ciudades pueden adaptarse para el bienestar de los animales. Nelly resalta la importancia de crear entornos inclusivos que fomenten la convivencia armoniosa entre humanos y mascotas.',
        },
        {
          titulo:
            'Oruro consciente: Programa de educación para promover la tenencia responsable de mascotas',
          imagen: 'n2.jpg',
          fechaPublicacion: '2023-03-18 15:28:39.572',
          lugar: 'Oruro',
          autor: 'Cinthya',
          contenido:
            'Cinthya comparte detalles sobre un programa de educación en Oruro que tiene como objetivo promover la tenencia responsable de mascotas. A través de talleres interactivos y campañas de concientización, la comunidad de Oruro trabaja para garantizar que los dueños de mascotas estén bien informados sobre las necesidades y responsabilidades que conlleva cuidar a un animal. Cinthya destaca cómo la educación puede ser clave para construir relaciones saludables entre humanos y mascotas.',
        },
        {
          titulo:
            'Postosi solidario: Campaña de vacunación gratuita para perros y gatos',
          imagen: 'n3.jpeg',
          fechaPublicacion: '2023-03-22 10:11:25.894',
          lugar: 'Postosi',
          autor: 'Ivan',
          contenido:
            'Ivan comparte la historia de una campaña de vacunación gratuita en Postosi, enfocada en proteger la salud de perros y gatos en la comunidad. A través de esta iniciativa, liderada por Ivan y un equipo de veterinarios voluntarios, se busca prevenir enfermedades y promover la importancia de la salud preventiva en las mascotas. La comunidad de Postosi responde positivamente a esta acción que demuestra el compromiso con el bienestar animal.',
        },
        {
          titulo:
            'Chuquisaca animal-friendly: Parques y eventos para fortalecer los lazos entre humanos y mascotas',
          imagen: 'n4.jpg',
          fechaPublicacion: '2023-03-27 04:49:53.123',
          lugar: 'Chuquisaca',
          autor: 'Alcides',
          contenido:
            'Alcides nos lleva a Chuquisaca, donde la comunidad ha trabajado en la creación de espacios y eventos que fortalecen los lazos entre humanos y mascotas. Desde parques adaptados hasta festivales pet-friendly, Chuquisaca se destaca como un lugar que valora la inclusión de los animales en la vida cotidiana. Alcides enfatiza cómo estas iniciativas contribuyen a una convivencia más armoniosa y feliz entre la población y sus mascotas.',
        },
        {
          titulo:
            'Campaña de esterilización en Santa Cruz: Reducción de la superpoblación de animales',
          imagen: 'n5.jpeg',
          fechaPublicacion: '2023-04-01 19:36:17.765',
          lugar: 'Santa Cruz',
          autor: 'Nelly',
          contenido:
            'Nelly comparte una segunda historia desde Santa Cruz, esta vez centrada en una exitosa campaña de esterilización. A través de entrevistas con veterinarios y dueños de mascotas, Nelly explora cómo esta iniciativa ha contribuido a la reducción de la superpoblación de animales en Santa Cruz. La comunidad reconoce la importancia de la esterilización para garantizar un equilibrio sostenible entre la población de animales y su bienestar.',
        },
        {
          titulo:
            'Convivencia en familia en Cochabamba: Adopción de mascotas como parte esencial del hogar',
          imagen: 'n6.jpeg',
          fechaPublicacion: '2023-04-06 08:02:44.321',
          lugar: 'Cochabamba',
          autor: 'Cinthya',
          contenido:
            'Cinthya nos presenta historias de adopciones en Cochabamba, destacando cómo la adopción de mascotas se ha convertido en una parte esencial de la vida familiar en la ciudad. A través de testimonios de adoptantes y detalles sobre programas de adopción, Cinthya explora cómo la presencia de mascotas ha enriquecido la vida de las familias en Cochabamba, fomentando la responsabilidad y el amor por los animales.',
        },
        {
          titulo:
            'Historias conmovedoras en Beni: Rescates de animales exóticos en peligro',
          imagen: 'n7.jpg',
          fechaPublicacion: '2023-04-11 23:45:29.154',
          lugar: 'Beni',
          autor: 'Ivan',
          contenido:
            'Ivan nos sumerge en historias conmovedoras de rescates de animales exóticos en peligro en la región de Beni. Desde la liberación de aves raras hasta la rehabilitación de felinos en peligro de extinción, la dedicación de Ivan y su equipo ha marcado la diferencia. La comunidad se une para apoyar la conservación de la biodiversidad y el respeto hacia todas las formas de vida en Beni.',
        },
        {
          titulo:
            'Refugio para mascotas en Pando: Alcides crea un santuario para animales abandonados',
          imagen: 'n8.jpg',
          fechaPublicacion: '2023-04-15 12:59:36.987',
          lugar: 'Pando',
          autor: 'Alcides',
          contenido:
            'Alcides comparte la inspiradora historia de la creación de un refugio para mascotas en Pando. Este santuario, fundado por Alcides y un grupo de amantes de los animales, brinda refugio y cuidado a perros y gatos abandonados. A través de relatos conmovedores de adopciones exitosas y la transformación de vidas, la comunidad se une para respaldar esta iniciativa que demuestra el poder del amor y la compasión.',
        },
        {
          titulo:
            'Eventos pet-friendly en Sucre: Nelly destaca las actividades para disfrutar con tus mascotas',
          imagen: 'n9.jpg',
          fechaPublicacion: '2023-04-20 18:07:50.632',
          lugar: 'Sucre',
          autor: 'Nelly',
          contenido:
            'Nelly nos sumerge en el vibrante mundo pet-friendly de Sucre, destacando eventos y actividades diseñadas para disfrutar con mascotas. Desde ferias de adopción hasta competencias de habilidades, Sucre se posiciona como un lugar donde la comunidad celebra la compañía de sus animales. Nelly destaca la importancia de crear lazos más fuertes entre humanos y mascotas a través de experiencias compartidas.',
        },
        {
          titulo:
            'Educación para dueños de mascotas en Oruro: Cinthya promueve prácticas responsables',
          imagen: 'n10.jpg',
          fechaPublicacion: '2023-04-25 07:33:12.876',
          lugar: 'Oruro',
          autor: 'Cinthya',
          contenido:
            'Cinthya nos presenta un programa educativo en Oruro centrado en promover prácticas responsables de tenencia de mascotas. A través de talleres interactivos y campañas de sensibilización, Cinthya y su equipo buscan empoderar a los dueños de mascotas con conocimientos sobre cuidados, alimentación y bienestar animal. La comunidad de Oruro abraza estas iniciativas, reconociendo la importancia de crear ambientes saludables para sus queridos compañeros peludos.',
        },
        {
          titulo:
            'Postosi comprometido: Rescate de animales heridos tras desastre natural',
          imagen: 'n1.jpg',
          fechaPublicacion: '2023-04-30 21:50:28.465',
          lugar: 'Postosi',
          autor: 'Ivan',
          contenido:
            'Ivan comparte la conmovedora historia de un equipo de rescate en Postosi que trabajó incansablemente para salvar a animales heridos después de un desastre natural. Desde perros atrapados en escombros hasta gatos perdidos, la comunidad de Postosi se unió para brindar apoyo y consuelo a estas mascotas afectadas. Ivan destaca la solidaridad y el sentido de comunidad que surge en momentos difíciles.',
        },
        {
          titulo:
            'Chuquisaca verde: Proyecto de reforestación impulsado por amantes de animales',
          imagen: 'n2.jpg',
          fechaPublicacion: '2024-01-15 09:45:21.123',
          lugar: 'Chuquisaca',
          autor: 'Alcides',
          contenido:
            'Alcides nos sumerge en un proyecto único en Chuquisaca, donde amantes de los animales se han unido para impulsar una iniciativa de reforestación. Este proyecto, liderado por Alcides y voluntarios, no solo busca restaurar el hábitat natural de diversas especies, sino que también pretende crear un entorno más saludable y sostenible para las mascotas y la fauna local. La comunidad aplaude esta acción que demuestra cómo la conservación y el bienestar animal están intrínsecamente conectados.',
        },
        {
          titulo:
            'Campaña de adopción en Santa Cruz: Nelly narra encuentros inolvidables',
          imagen: 'n3.jpeg',
          fechaPublicacion: '2024-01-20 16:32:45.789',
          lugar: 'Santa Cruz',
          autor: 'Nelly',
          contenido:
            'Nelly nos lleva a una emotiva jornada de adopción en Santa Cruz, narrando encuentros inolvidables entre animales y sus nuevos dueños. Desde cachorros juguetones hasta gatos cariñosos, la comunidad de Santa Cruz abraza la alegría de dar un hogar a mascotas necesitadas. Nelly destaca la importancia de la adopción como un acto de amor que cambia vidas.',
        },
        {
          titulo:
            'Cochabamba canina: Evento de belleza y destrezas para perros',
          imagen: 'n4.jpg',
          fechaPublicacion: '2024-01-25 22:18:33.456',
          lugar: 'Cochabamba',
          autor: 'Cinthya',
          contenido:
            'Cinthya nos invita a un evento especial en Cochabamba, donde dueños de perros muestran la belleza y destrezas de sus mascotas. Desde desfiles de moda canina hasta competiciones de agilidad, este evento destaca la diversidad y el talento de los perros de Cochabamba. Cinthya enfatiza cómo estos eventos no solo son entretenidos, sino que también fortalecen los lazos entre dueños y sus fieles compañeros.',
        },
        {
          titulo:
            'Postosi solidario: Jornada de alimentación para perros callejeros',
          imagen: 'n5.jpeg',
          fechaPublicacion: '2024-01-30 07:59:12.890',
          lugar: 'Postosi',
          autor: 'Ivan',
          contenido:
            'Ivan nos lleva a una jornada solidaria en Postosi, donde la comunidad se reunió para llevar a cabo una jornada de alimentación para perros callejeros. Con la participación de voluntarios y donaciones de alimentos, se logró brindar un día de comida y cariño a los animales desamparados. Ivan destaca cómo pequeños actos pueden tener un impacto significativo en la calidad de vida de los perros callejeros.',
        },
        {
          titulo:
            'Pando verde: Parques ecológicos para el esparcimiento de mascotas y dueños',
          imagen: 'n6.jpeg',
          fechaPublicacion: '2024-02-05 13:44:58.321',
          lugar: 'Pando',
          autor: 'Alcides',
          contenido:
            'Alcides comparte la historia de la creación de parques ecológicos en Pando, diseñados especialmente para el esparcimiento de mascotas y sus dueños. Estos espacios verdes no solo promueven la actividad física y el juego entre las mascotas, sino que también fomentan la conexión entre la comunidad. Alcides destaca la importancia de proporcionar entornos saludables y seguros para el disfrute de todos.',
        },
        {
          titulo:
            'Santa Cruz consciente: Campaña de esterilización para controlar la población felina',
          imagen: 'n7.jpg',
          fechaPublicacion: '2024-02-10 18:21:37.654',
          lugar: 'Santa Cruz',
          autor: 'Nelly',
          contenido:
            'Nelly nos informa sobre una campaña de esterilización en Santa Cruz dirigida específicamente a controlar la población felina. A través de entrevistas con veterinarios y dueños de gatos, Nelly explora cómo esta iniciativa contribuye a evitar el aumento descontrolado de gatos callejeros. La comunidad reconoce la importancia de la esterilización como una medida clave para el bienestar de los felinos y la convivencia armoniosa.',
        },
        {
          titulo:
            'Cochabamba solidaria: Donaciones de alimentos para refugios de mascotas',
          imagen: 'n8.jpg',
          fechaPublicacion: '2024-02-15 03:12:49.984',
          lugar: 'Cochabamba',
          autor: 'Cinthya',
          contenido:
            'Cinthya nos cuenta sobre la solidaridad en Cochabamba, donde la comunidad se une para realizar donaciones de alimentos a refugios de mascotas. La generosidad de los ciudadanos ha permitido garantizar que los animales en refugios tengan acceso a una dieta equilibrada. Cinthya destaca cómo la comunidad de Cochabamba demuestra que todos pueden contribuir de alguna manera al bienestar de los animales sin hogar.',
        },
      ];
      const noticiasCreadas = await this.noticiaRepository.save(noticiasData);
      return noticiasCreadas;
    } catch (error) {
      throw new InternalServerErrorException({
        statusCode: 500,
        error: `Error del Servidor en (Noticia método seedCreate): ${error}`,
        message: `Error del Servidor en (Noticia método seedCreate): ${error}`,
      });
    }
  }

  async findAll(): Promise<Noticia[]> {
    try {
      const noticias = await this.noticiaRepository.find();
      if (noticias.length === 0) {
        throw new BadRequestException({
          statusCode: 400,
          error: `No se encontraron Noticias.`,
          message: `No se encontraron Noticias. A un no se subio.`,
        });
      }
      return noticias;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia metodo findAll): ${error}`,
          message: `Error del Servidor en (Noticia metodo findAll): ${error}`,
        });
      }
    }
  }
  async findAllFechasRecientes(): Promise<Noticia[]> {
    try {
      const noticias = await this.noticiaRepository.find({
        order: {
          fechaPublicacion: 'DESC',
        },
      });

      if (noticias.length === 0) {
        throw new BadRequestException({
          statusCode: 400,
          error: `No se encontraron Noticias.`,
          message: `No se encontraron Noticias. Aún no se han subido noticias.`,
        });
      }

      return noticias;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia método findAllFechasRecientes): ${error}`,
          message: `Error del Servidor en (Noticia método findAllFechasRecientes): ${error}`,
        });
      }
    }
  }
  async findAllFechasAmtiguas(): Promise<Noticia[]> {
    try {
      const noticias = await this.noticiaRepository.find({
        order: {
          fechaPublicacion: 'ASC',
        },
      });

      if (noticias.length === 0) {
        throw new BadRequestException({
          statusCode: 400,
          error: `No se encontraron Noticias.`,
          message: `No se encontraron Noticias. Aún no se han subido noticias.`,
        });
      }

      return noticias;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia método findAllFechasAmtiguas): ${error}`,
          message: `Error del Servidor en (Noticia método findAllFechasAmtiguas): ${error}`,
        });
      }
    }
  }

  async findOne(id: number): Promise<Noticia> {
    try {
      const noticia = await this.noticiaRepository.findOne({ where: { id } });
      if (!noticia) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Noticia con ID ${id} NO Existe`,
          message: `La Noticia con ID ${id} no fue encontrado`,
        });
      }
      return noticia;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia metodo findOne): ${error}`,
          message: `Error del Servidor en (Noticia metodo findOne): ${error}`,
        });
      }
    }
  }
  async findLugar(lugar: string): Promise<Noticia[]> {
    try {
      const noticia = await this.noticiaRepository.find({
        where: { lugar: lugar },
      });
      if (!noticia) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Noticia en el lugar: ${lugar} NO Existe`,
          message: `La Noticia en el lugar: ${lugar} no fue encontrado`,
        });
      }
      return noticia;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia metodo findOne): ${error}`,
          message: `Error del Servidor en (Noticia metodo findOne): ${error}`,
        });
      }
    }
  }
  async findOneTitulo(titulo: string): Promise<Noticia[]> {
    try {
      const noticia = await this.noticiaRepository.find({
        where: { titulo: Like(`%${titulo}%`) },
      });
      if (!noticia) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Noticia con Titulo ${titulo} NO Existe`,
          message: `La Noticia con Titulo ${titulo} no fue encontrado`,
        });
      }
      return noticia;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia metodo findOne): ${error}`,
          message: `Error del Servidor en (Noticia metodo findOne): ${error}`,
        });
      }
    }
  }
  async findAutor(autor: string): Promise<Noticia[]> {
    try {
      const noticia = await this.noticiaRepository.find({
        where: { autor: Like(`%${autor}%`) },
      });
      if (!noticia) {
        throw new BadRequestException({
          statusCode: 400,
          error: `La Noticia con Autor ${autor} NO Existe`,
          message: `La Noticia con Autor ${autor} no fue encontrado`,
        });
      }
      return noticia;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia metodo findOne): ${error}`,
          message: `Error del Servidor en (Noticia metodo findOne): ${error}`,
        });
      }
    }
  }
  async update(
    id: number,
    updateNoticiaDto: UpdateNoticiaDto,
  ): Promise<Noticia> {
    try {
      const noticia = await this.findOne(id);
      this.noticiaRepository.merge(noticia, updateNoticiaDto);
      const actualizarNoticia = await this.noticiaRepository.save(noticia);
      return actualizarNoticia;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia método update): ${error}`,
          message: `Error del Servidor en (Noticia método update): ${error}`,
        });
      }
    }
  }
  async remove(id: number): Promise<Noticia> {
    try {
      const noticia = await this.findOne(id);
      const noticiaEliminada = { ...noticia };
      await this.noticiaRepository.remove(noticia);
      return noticiaEliminada;
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error;
      } else {
        throw new InternalServerErrorException({
          statusCode: 500,
          error: `Error del Servidor en (Noticia método remove): ${error}`,
          message: `Error del Servidor en (Noticia método remove): ${error}`,
        });
      }
    }
  }
}
