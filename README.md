1. es importante ejecutar post para crear la semilla y tener datos con los que trabajar

2. las url que realice son las siguientes
   2.1 http://localhost:3000/noticia/
   donde ejecuta el Get metodo findAll donde devuelve todo los datos

2.2 http://localhost:3000/noticia/
donde ejecuta el Post metodo create donde se realizan la creacion de noticias

2.3 http://localhost:3000/noticia/seedcreate
donde ejecuta el Post metodo seedCreate este metodo es muy importante ya que es la semilla para cargar noticias

2.4 http://localhost:3000/noticia/recientes
donde ejecuta el Get metodo findAllFechasRecientes donde retorna los las noticias con fecha mas reciente

2.5 http://localhost:3000/noticia/antiguas
donde ejecuta el Get metodo findAllFechasAmtigua donde retorna los las noticias con fecha mas antigua

2.6 http://localhost:3000/noticia/:id
donde ejecuta el Get metodo findOne donde retorna los la noticia con con id introducido

2.7 http://localhost:3000/noticia/lugar/:lugar
donde ejecuta el Get metodo findLugar donde retorna los las noticias con el lugar creado (los nueve departamentos a eleccion)

2.8 http://localhost:3000/noticia/titulo/:titulo
donde ejecuta el Get metodo findOneTitulo donde retorna los las noticias con el titulo introducido

2.9 http://localhost:3000/noticia/autor/:autor
donde ejecuta el Get metodo findAutor donde retorna los las noticias con el autor introducido

2.12 http://localhost:3000/noticia/:id
donde ejecuta el Patch metodo update donde se actualizara datos de la noticia se requiere el id

2.13 http://localhost:3000/noticia/:id
donde ejecuta el Delete metodo remove donde se liminara la noticia se requiere el id

nota 1:
reemplaza :id por un numero de id
reemplaza :lugar por un texto
reemplaza :titulo por un texto

para la instalacion y configuracuon ir al archivo: NSTALL.md
