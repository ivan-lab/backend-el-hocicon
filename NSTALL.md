1. Verificar si esta activo el servicio de PostgreSQL
2. Crear la base de datos elhocicon
3. Una ves realizado el git clone entrar a la carpeta del proyecto
   "backend-el-hocicon"
4. Ejecutar npm install o pnpm install o yarn install
5. Verificar la variable de entorno .env, si las configuracuones
   no son las de su PC cambia las configuraciones deacuerdo a su pc
6. Ejecutar la aplicacion para el entorno de desarrollo con
   npm run start:dev
7. verificar la consola por si surgen errores
8. En postman o cualquier otro gestor realizar el post semilla
   http://localhost:3000/noticia/seedcreate
   donde se cargaran los datos de prueba
9. la aplicacion correra en la url: http://localhost:3000
